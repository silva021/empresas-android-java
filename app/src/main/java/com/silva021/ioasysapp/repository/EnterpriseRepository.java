package com.silva021.ioasysapp.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.silva021.ioasysapp.model.EnterpriseList;
import com.silva021.ioasysapp.api.EnterpriseService;
import com.silva021.ioasysapp.api.RetrofitFactory;
import com.silva021.ioasysapp.utils.constant.Constant;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterpriseRepository {

    public LiveData<EnterpriseList> getEnterprises(Map<String, String> map) {
        final MutableLiveData<EnterpriseList> data = new MutableLiveData<>();
        RetrofitFactory.createService(EnterpriseService.class).getListEnterprises(map).enqueue(new Callback<EnterpriseList>() {
            @Override
            public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {
                if ((response.code() == 200)) {
                    data.setValue(response.body());
                } else if ((response.code() == 401) || (response.code() == 404))
                    data.setValue(null);
            }

            @Override
            public void onFailure(Call<EnterpriseList> call, Throwable t) {
                Log.e(Constant.TAG_ERROR_ENTERPRISE_REPOSITORY, t.getMessage());
            }
        });

        return data;
    }

    public LiveData<EnterpriseList> getEnterprisesByName(Map<String, String> map, String name) {
        final MutableLiveData<EnterpriseList> data = new MutableLiveData<>();
        RetrofitFactory.createService(EnterpriseService.class).getListEnterprisesByName(map, name).enqueue(new Callback<EnterpriseList>() {
            @Override
            public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {
                if ((response.code() == 200)) {
                    data.setValue(response.body());
                } else if ((response.code() == 401) || (response.code() == 404))
                    data.setValue(null);
            }

            @Override
            public void onFailure(Call<EnterpriseList> call, Throwable t) {
                Log.e(Constant.TAG_ERROR_ENTERPRISE_REPOSITORY, t.getMessage());
            }
        });

        return data;
    }
}
