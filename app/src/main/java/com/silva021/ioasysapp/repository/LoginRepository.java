package com.silva021.ioasysapp.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.silva021.ioasysapp.model.LoginRequest;
import com.silva021.ioasysapp.model.LoginResponse;
import com.silva021.ioasysapp.model.UserCredentials;
import com.silva021.ioasysapp.utils.constant.Constant;
import com.silva021.ioasysapp.utils.preferences.MySharedPreferences;
import com.silva021.ioasysapp.api.LoginService;
import com.silva021.ioasysapp.api.RetrofitFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {
    private Context context;

    public LoginRepository(Context context) {
        this.context = context;
    }

    public LiveData<LoginResponse> signIn(LoginRequest body) {
        final MutableLiveData<LoginResponse> data = new MutableLiveData<>();
        RetrofitFactory.createService(LoginService.class).signIn(body).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if ((response.code() == 200)) {
                    data.setValue(response.body());
                    saveCredentialsPreferences(response);
                } else if ((response.code() == 401) || (response.code() == 404))
                    data.setValue(null);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(Constant.TAG_ERROR_LOGIN_REPOSITORY, t.getMessage());
            }
        });
        return data;
    }


    public LoginResponse signInTest(LoginRequest body) {
        final LoginResponse[] data = new LoginResponse[1];
        RetrofitFactory.createService(LoginService.class).signIn(body).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if ((response.code() == 200)) {
                    data[0] = response.body();
                    saveCredentialsPreferences(response);
                } else if ((response.code() == 401) || (response.code() == 404))
                    data[0] = null;
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(Constant.TAG_ERROR_LOGIN_REPOSITORY, t.getMessage());
            }
        });
        return data[0];
    }

    void saveCredentialsPreferences(Response<LoginResponse> loginResponse) {
        String uid = loginResponse.headers().get("uid");
        String accessToken = loginResponse.headers().get("access-token");
        String client = loginResponse.headers().get("client");
        new MySharedPreferences(context).setCredentials(new UserCredentials(uid, accessToken, client));
    }
}
