package com.silva021.ioasysapp.api;

import com.silva021.ioasysapp.model.LoginRequest;
import com.silva021.ioasysapp.model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {
//    @FormUrlEncoded
    @POST("users/auth/sign_in")
//    Call<LoginResponse> signIn(@Field("email") String email, @Field("password") String password );
    Call<LoginResponse> signIn(@Body LoginRequest loginRequest);
}
