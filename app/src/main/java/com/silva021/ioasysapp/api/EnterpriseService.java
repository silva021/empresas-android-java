package com.silva021.ioasysapp.api;

import com.silva021.ioasysapp.model.EnterpriseList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface EnterpriseService {
    @GET("enterprises")
    Call<EnterpriseList> getListEnterprises(@QueryMap Map<String, String> map);

    @GET("enterprises")
    Call<EnterpriseList> getListEnterprisesByName(@QueryMap Map<String, String> map, @Query("name") String name);
}
