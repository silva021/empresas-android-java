package com.silva021.ioasysapp.api;

import com.silva021.ioasysapp.utils.constant.Constant;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {

    private static final Retrofit.Builder mBuilder = new Retrofit.Builder()
            .baseUrl(Constant.BASE_URL + Constant.VERSION_API)
            .addConverterFactory(GsonConverterFactory.create());

    private static final Retrofit mRetrofit = mBuilder.build();


    public static <T> T createService(Class<T> classService) {
        return mRetrofit.create(classService);
    }
}
