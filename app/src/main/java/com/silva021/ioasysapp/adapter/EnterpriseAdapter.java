package com.silva021.ioasysapp.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.silva021.ioasysapp.utils.constant.Constant;
import com.silva021.ioasysapp.databinding.LayoutCompanyBinding;
import com.silva021.ioasysapp.model.Enterprise;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.ViewHolder> {
    private Context mContext;
    private List<Enterprise> mEnterpriseList = new ArrayList<>();
    private OnItemClickListener mListener;

    public EnterpriseAdapter(Context mContext) {
        this.mContext = mContext;
    }
    public void setData(List<Enterprise> data){
        mEnterpriseList.clear();
        mEnterpriseList.addAll(data);
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutCompanyBinding.inflate(LayoutInflater.from(mContext), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Enterprise company = mEnterpriseList.get(position);

        holder.mBinding.txtNameCompany.setText(company.getEnterpriseName());
        holder.mBinding.txtCountryCompany.setText(company.getCountry());
        holder.mBinding.txtTypeCompany.setText(company.getEnterpriseType().getEnterpriseTypeName());

        if (!company.getPhoto().isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.mBinding.imgCompany.setClipToOutline(true);
            }
            Glide.with(mContext).load(Constant.BASE_URL + company.getPhoto()).into(holder.mBinding.imgCompany);
        }
    }

    public Enterprise returnCompanyPosition(int position) {
        return mEnterpriseList.get(position);
    }

    @Override
    public int getItemCount() {
        return mEnterpriseList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LayoutCompanyBinding mBinding;

        public ViewHolder(@NonNull LayoutCompanyBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;

            if (mListener != null)
                mBinding.layoutCard.setOnClickListener(v -> mListener.onItemClick(returnCompanyPosition(getAdapterPosition())));

        }
    }

    public interface OnItemClickListener {
        void onItemClick(Enterprise enterprise);
    }
}
