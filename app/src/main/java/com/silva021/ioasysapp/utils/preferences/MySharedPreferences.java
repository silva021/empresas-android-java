package com.silva021.ioasysapp.utils.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.silva021.ioasysapp.model.UserCredentials;

import java.util.HashMap;
import java.util.Map;

import static com.silva021.ioasysapp.utils.constant.Constant.FILE_PREFERENCES;
import static com.silva021.ioasysapp.utils.constant.Constant.KEY_ACCESS_TOKEN;
import static com.silva021.ioasysapp.utils.constant.Constant.KEY_CLIENT;
import static com.silva021.ioasysapp.utils.constant.Constant.KEY_UID;

public class MySharedPreferences {
    Context context;
    SharedPreferences.Editor preferences;
    SharedPreferences sharedPreferences;

    public MySharedPreferences(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(FILE_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setCredentials(UserCredentials credentials) {
        preferences = sharedPreferences.edit();
        preferences.putString(KEY_ACCESS_TOKEN, credentials.accessToken);
        preferences.putString(KEY_UID, credentials.uid);
        preferences.putString(KEY_CLIENT, credentials.client);
        preferences.apply();
    }

    public Map<String, String> getHashMapCredentials() {
        Map<String, String> mapCredentials = new HashMap<>();
        mapCredentials.put(KEY_ACCESS_TOKEN, sharedPreferences.getString(KEY_ACCESS_TOKEN, null));
        mapCredentials.put(KEY_UID, sharedPreferences.getString(KEY_UID, null));
        mapCredentials.put(KEY_CLIENT, sharedPreferences.getString(KEY_CLIENT, null));
        return mapCredentials;
    }

    public UserCredentials getCredentials() {
        return new UserCredentials(sharedPreferences.getString(KEY_UID, null), sharedPreferences.getString(KEY_ACCESS_TOKEN, null), sharedPreferences.getString(KEY_CLIENT, null));
    }


}
