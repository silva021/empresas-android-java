package com.silva021.ioasysapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.silva021.ioasysapp.R;

public class MyProgressDialog {
    private Activity activity;
    private AlertDialog alertDialog;


    public MyProgressDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {
        alertDialog = new AlertDialog.Builder(activity).setView(activity.getLayoutInflater().inflate(R.layout.progress_dialog, null)).setCancelable(false).create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
    }

    public void closeDialog() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }
}
