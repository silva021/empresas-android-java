package com.silva021.ioasysapp.utils.constant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constant {
    public static final String BASE_URL = "https://empresas.ioasys.com.br";
    public static final String VERSION_API = "/api/v1/";
    public static final String FILE_PREFERENCES = "user_config";
    public static final String KEY_ACCESS_TOKEN = "access-token";
    public static final String KEY_CLIENT = "client";
    public static final String KEY_UID = "uid";
    public static final String TAG_ERROR_LOGIN_REPOSITORY = "loginRepository";
    public static final String TAG_ERROR_ENTERPRISE_REPOSITORY = "mainRepository";


    public static <T> void openActivity(Activity activity, Class<T> classActivity) {
        activity.startActivity(new Intent(activity, classActivity));
        activity.finish();
    }

    public static boolean isConnectedInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo info = cm.getActiveNetworkInfo();

            return (info != null && info.isConnected());
        }
        return false;
    }
}
