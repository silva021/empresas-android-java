package com.silva021.ioasysapp.model;
public class UserCredentials {

    public String uid;

    public String accessToken;

    public String client;

    public UserCredentials(String uid, String accessToken, String client) {
        this.uid = uid;
        this.accessToken = accessToken;
        this.client = client;
    }
}
