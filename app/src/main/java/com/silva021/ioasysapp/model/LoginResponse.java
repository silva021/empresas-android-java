package com.silva021.ioasysapp.model;

public class LoginResponse {

    private Investor investor;
    private boolean success;
    private String enterprise;

    public LoginResponse(Investor investor, boolean success, String enterprise) {
        this.investor = investor;
        this.success = success;
        this.enterprise = enterprise;
    }

    public Investor getInvestor() {
        return investor;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getEnterprise() {
        return enterprise;
    }
}
