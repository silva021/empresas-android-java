package com.silva021.ioasysapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EnterpriseList implements Serializable {
    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprise;
    private boolean success;


    public List<Enterprise> getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(List<Enterprise> enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Enterprise> getEnterprises() {
        return enterprise;
    }
}
