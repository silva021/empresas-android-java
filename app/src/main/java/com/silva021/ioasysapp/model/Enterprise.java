package com.silva021.ioasysapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Enterprise implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email_enterprise")
    @Expose
    private String emailEnterprise;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("own_enterprise")
    @Expose
    private Boolean ownEnterprise;
    @SerializedName("enterprise_name")
    @Expose
    private String enterpriseName;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("share_price")
    @Expose
    private Integer sharePrice;
    @SerializedName("enterprise_type")
    @Expose
    private EnterpriseType enterpriseType;

    public Integer getId() {
        return id;
    }

    public String getEmailEnterprise() {
        return emailEnterprise;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public Boolean getOwnEnterprise() {
        return ownEnterprise;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDescription() {
        return description;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public Integer getValue() {
        return value;
    }

    public Integer getSharePrice() {
        return sharePrice;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }
}
