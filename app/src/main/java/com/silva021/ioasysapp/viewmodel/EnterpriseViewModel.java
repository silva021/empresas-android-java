package com.silva021.ioasysapp.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.silva021.ioasysapp.model.EnterpriseList;
import com.silva021.ioasysapp.utils.preferences.MySharedPreferences;
import com.silva021.ioasysapp.repository.EnterpriseRepository;

public class EnterpriseViewModel extends AndroidViewModel {
    private final Context context;
    private EnterpriseRepository enterpriseRepository;

    public EnterpriseViewModel(@NonNull Application application) {
        super(application);
        enterpriseRepository = new EnterpriseRepository();
        this.context = application.getApplicationContext();
    }

    public LiveData<EnterpriseList> getListEnterprise() {
        return enterpriseRepository.getEnterprises(new MySharedPreferences(context).getHashMapCredentials());
    }

    public LiveData<EnterpriseList> getListEnterpriseByName(String name) {
        return enterpriseRepository.getEnterprisesByName(new MySharedPreferences(context).getHashMapCredentials(), name);
    }
}
