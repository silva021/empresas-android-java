package com.silva021.ioasysapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.silva021.ioasysapp.model.LoginRequest;
import com.silva021.ioasysapp.model.LoginResponse;
import com.silva021.ioasysapp.repository.LoginRepository;

public class LoginViewModel extends AndroidViewModel {

    private LoginRepository loginRepository;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        loginRepository = new LoginRepository(getApplication().getApplicationContext());
    }

    public LiveData<LoginResponse> signIn(LoginRequest body) {
        return loginRepository.signIn(body);
    }
}
