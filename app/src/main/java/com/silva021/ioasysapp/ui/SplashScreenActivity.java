package com.silva021.ioasysapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;

import com.silva021.ioasysapp.R;
import com.silva021.ioasysapp.databinding.ActivitySplashScreenBinding;

import static com.silva021.ioasysapp.utils.constant.Constant.openActivity;

public class SplashScreenActivity extends AppCompatActivity {
    ActivitySplashScreenBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivitySplashScreenBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        startAnimation();
        new Handler().postDelayed(() -> openActivity(this, LoginActivity.class), 2000);

    }

    private void startAnimation() {mBinding.imgLogo.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.heart_animation));}



}