package com.silva021.ioasysapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.SearchView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.silva021.ioasysapp.R;
import com.silva021.ioasysapp.adapter.EnterpriseAdapter;
import com.silva021.ioasysapp.databinding.ActivityMainBinding;
import com.silva021.ioasysapp.model.Enterprise;
import com.silva021.ioasysapp.utils.constant.Constant;
import com.silva021.ioasysapp.viewmodel.EnterpriseViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements EnterpriseAdapter.OnItemClickListener {
    private ActivityMainBinding mBinding;
    private EnterpriseAdapter enterpriseAdapter;
    private EnterpriseViewModel mEnterpriseViewModel;
    private ArrayList<Enterprise> mListEnterprises = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        setSupportActionBar(mBinding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("");

        // View Model
        mEnterpriseViewModel = new ViewModelProvider(this).get(EnterpriseViewModel.class);

        initializeRecycler();
    }

    private void initializeRecycler() {
        enterpriseAdapter = new EnterpriseAdapter(getApplicationContext());
        enterpriseAdapter.setData(mListEnterprises);
        enterpriseAdapter.setOnClickListener(this);
        mBinding.recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mBinding.recycler.setAdapter(enterpriseAdapter);
    }

    private void showRecycler() {
        mBinding.recycler.setVisibility(View.VISIBLE);
        mBinding.txtMessageNotFound.setVisibility(View.GONE);
        mBinding.txtMessageInitialize.setVisibility(View.GONE);
    }

    void showMessage(boolean isNotFound) {
        mBinding.recycler.setVisibility(View.GONE);
        mBinding.txtMessageInitialize.setVisibility(isNotFound ? View.GONE : View.VISIBLE);
        mBinding.txtMessageNotFound.setVisibility(isNotFound ? View.VISIBLE : View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search_view));

        searchView.setOnSearchClickListener(v -> mBinding.layoutLogo.setVisibility(View.GONE));

        searchView.setOnCloseListener(() -> {
            mBinding.layoutLogo.setVisibility(View.VISIBLE);
            showMessage(false);
            return false;
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (Constant.isConnectedInternet(getApplicationContext())) {
                    mEnterpriseViewModel.getListEnterpriseByName(query).observe(MainActivity.this, enterprises -> {
                        if (!enterprises.getEnterprises().isEmpty())
                            updateRecyclerView(enterprises.getEnterprise());
                        else
                            showMessage(enterprises.getEnterprises().isEmpty());
                    });
                    mBinding.layoutLogo.setVisibility(View.VISIBLE);
                } else {
                    showMessageInternet();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mBinding.layoutLogo.setVisibility(View.GONE);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onItemClick(Enterprise enterprise) {
        startActivity(new Intent(this, DetailsActivity.class).putExtra("company", enterprise));
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
    }

    private void showMessageInternet() {
        new Handler().postDelayed(() -> {
            Snackbar.make(getCurrentFocus(), "Seu dispositivo não está connectado a nenhuma rede, tente novamente", Snackbar.LENGTH_LONG).setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
        }, 1000);
    }
    void updateRecyclerView(List<Enterprise> enterpriseList) {
        enterpriseAdapter.setData(enterpriseList);
        showRecycler();
    }
}