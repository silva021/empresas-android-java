package com.silva021.ioasysapp.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.silva021.ioasysapp.R;
import com.silva021.ioasysapp.utils.constant.Constant;
import com.silva021.ioasysapp.databinding.ActivityDetailsBinding;
import com.silva021.ioasysapp.model.Enterprise;

import java.util.Objects;

public class DetailsActivity extends AppCompatActivity {
    Enterprise mEnterprise;
    ActivityDetailsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityDetailsBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        if (getIntent().getExtras() != null) {
            mEnterprise = (Enterprise) getIntent().getExtras().getSerializable("company");
        }

        initializeComponents();
    }

    private void initializeComponents() {
        setSupportActionBar(mBinding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        initializeBindingEnterpriseData();
    }

    private void initializeBindingEnterpriseData() {
        Objects.requireNonNull(getSupportActionBar()).setTitle(mEnterprise.getEnterpriseName());
        Glide.with(getApplicationContext()).load(Constant.BASE_URL + mEnterprise.getPhoto()).into(mBinding.imgLogoCompany);
        mBinding.txtDescriptionCompany.setText(mEnterprise.getDescription());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
            finish();
            return true;
        }
        return false;
    }
}