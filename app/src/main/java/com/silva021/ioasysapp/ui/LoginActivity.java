package com.silva021.ioasysapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.silva021.ioasysapp.R;
import com.silva021.ioasysapp.databinding.ActivityLoginBinding;
import com.silva021.ioasysapp.model.LoginRequest;
import com.silva021.ioasysapp.utils.MyProgressDialog;
import com.silva021.ioasysapp.utils.constant.Constant;
import com.silva021.ioasysapp.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding mBinding;
    LoginViewModel loginViewModel;
    MyProgressDialog dialog = new MyProgressDialog(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        mBinding.btnLogin.setOnClickListener(v -> signIn());
    }

    private void signIn() {
        dialog.showDialog();
        mBinding.txtMessageError.setVisibility(View.GONE);
        if (Constant.isConnectedInternet(getApplicationContext())) {
            loginViewModel.signIn(returnCredentialsUI()).observe(this, loginResponse -> {
                if (loginResponse != null)
                    new Handler().postDelayed(() -> Constant.openActivity(LoginActivity.this, MainActivity.class), 2000);
                else
                    showMessageLoginUnauthorized();
                dialog.closeDialog();
            });
        } else {
            showMessageInternet();
        }


    }

    private void showMessageInternet() {
        new Handler().postDelayed(() -> {
            Snackbar.make(getCurrentFocus(), "Seu dispositivo não está connectado a nenhuma rede, tente novamente", Snackbar.LENGTH_LONG).setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
            dialog.closeDialog();
        }, 2000);
    }

    private LoginRequest returnCredentialsUI() {
        return new LoginRequest(mBinding.edtEmail.getText().toString(), mBinding.edtPassword.getText().toString());
    }

    private void showMessageLoginUnauthorized() {
        mBinding.txtMessageError.setVisibility(View.VISIBLE);
        mBinding.btnLogin.setBackgroundColor(getResources().getColor(R.color.color_button_error));
    }



}