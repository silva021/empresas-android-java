![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### Visão geral do Projeto ###

* Neste repositório irá encontrar a aplicação feita na linguagem Java utilizando uma arquitetura baseada no MVVM + CLEAN

### Informações Importantes ###

#### Bibliotecas utilizadas ####
* Glide - Optei por usar pois é a biblioteca mais fácil para fazer o download de imagens e cachear.
* Gson - Optei por usar pois é a biblioteca mais fácil para converter objetos.
* Retrofit - Optei por usar pois é a biblioteca mais fácil de construir a conexão com um WebService.
* LiveData + ViewModel - Optei usar essas bibliotecas pois quis utilizar a arquitetura MVVM para construir a aplicação.

#### O que eu teria feito se tivesse mais tempo ####

* Testes unitários
* Injeção de dependência 
* Costumizaria o Layout

#### Como executar a aplicação ####

* Ao clicar na aplicação esperando 4 segundos a tela de Login será retornada
* Ao acessar com as credentiais corretas será aberta a tela para que possa pesquisar as Empresas pela barra de pesquisa.
* Ao escrever o nome na barra de pesquisa ao clicar no "enviar" será retornado a resposta se foi encontrado ou não.
